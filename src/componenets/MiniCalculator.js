import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      result: 0
    }
    this.addone = this.addone.bind(this);
    this.minusone = this.minusone.bind(this);
    this.doubleResult = this.doubleResult.bind(this);
    this.halfResult = this.halfResult.bind(this);
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <button onClick={this.addone}>加1</button>
          <button onClick={this.minusone}>减1</button>
          <button onClick={this.doubleResult}>乘以2</button>
          <button onClick={this.halfResult}>除以2</button>
        </div>
      </section>
    );
  }

  addone() {
    this.setState({result:this.state.result+1})
  }

  minusone() {
    this.setState({result:this.state.result-1})
  }

  doubleResult() {
    this.setState({result:this.state.result*2})
  }

  halfResult() {
    this.setState({result:this.state.result/2})
  }
}

export default MiniCalculator;

